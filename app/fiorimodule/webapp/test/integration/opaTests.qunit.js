/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"mg/cap/prov/sa/fiorimodule/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});
