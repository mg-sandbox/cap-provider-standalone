sap.ui.define([
    "sap/ui/core/Control"

], function (Control) {
    "use strict";
    return Control.extend("mg.cap.prov.sa.fiorimodule.control.goodbyeworld", {
        metadata: {
            properties: {},
            aggregations: {},
        },
        renderer: function (oRm, oControl) {
            console.info("Loaded: mg.cap.prov.sa.fiorimodule.control.goodbyeworld");
            oRm.write("<span>Goodbye World from Provider Application, see you next time...</span>");
        }
    });
});
