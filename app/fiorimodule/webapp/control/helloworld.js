sap.ui.define([
    "sap/ui/core/Control"

], function (Control) {
    "use strict";
    return Control.extend("mg.cap.prov.sa.fiorimodule.control.helloworld", {
        metadata: {
            properties: {},
            aggregations: {},
        },
        renderer: function (oRm, oControl) {
            console.info("Loaded: mg.cap.prov.sa.fiorimodule.control.helloworld");
            oRm.write("<span>Hello World from Provider Application</span>");
        }
    });
});
