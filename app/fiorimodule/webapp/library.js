sap.ui.define([], function() {
    "use strict";

    jQuery.sap.declare("mg.cap.prov.sa");

    sap.ui.getCore().initLibrary({

        name: "mg.cap.prov.sa",
        version: "1.0.0",
        dependencies: ["sap.ui.core"],
        types: [],
        interfaces: [],
        controls: [
           "mg.cap.prov.sa.control.helloworld" 
        ],
        elements: [],
        noLibraryCSS: true
    });
    
	console.log("mg.cap.prov.sa library loaded.");

    return mg.cap.prov.sa;

}, false);